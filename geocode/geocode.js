const request = require('request');

//Im still more like default functions than arrows
function geocodeback(address, callback) {
    var encodedAddress = encodeURIComponent(address);
    request({
        url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=AIzaSyCgZsXBjz9jlzZpDQutB1iuiNN6bWrJJ3Q`,
        json: true
    }, (error, response, body) => {
        if (error) {
            callback('Unable to connect to google serivce');
        } else if (body.status === 'ZERO_RESULTS') {
            callback('Unable to find that adress');
        } else if (body.status === 'OK') {
            callback(undefined, {
                Address: body.results[0].formatted_address,
                Latitude: body.results[0].geometry.location.lat,
                Longitude: body.results[0].geometry.location.lng,
            })
        ;}
    });
};

module.exports = {
    geocodeback
};