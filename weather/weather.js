const request = require('request');



function getWeather(lt, lg, callback){
    request(
        {
            url: `https://api.darksky.net/forecast/af1a15a527b00a1d367d16bef61188fc/${lt},${lg}`,
            json: true
        },
        (error, response, body) => {
            if (!error && response.statusCode ===200) {
                callback(undefined, {
                WeatherType: body.currently.precipType,
                Temperature :((5 / 9) * (body.currently.temperature - 32)).toFixed(2),
                apparentTemperature: ((5 / 9) * (body.currently.apparentTemperature - 32)).toFixed(2),

            });
            } else {
                callback('Unable to fetch weather');
        }; }
    );
};

module.exports.getWeather = getWeather;