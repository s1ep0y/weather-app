const axios = require ('axios')
const yargs = require('yargs');

console.log('________Starting Workd__________')

const argv = yargs
    .options({
        a: {
            demand: true,
            alias : 'address',
            describe: 'Adress to fetch weater for',
            string: true,
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

    var encodedAddress = encodeURIComponent(argv.address);
    var geocodeURL = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=AIzaSyCgZsXBjz9jlzZpDQutB1iuiNN6bWrJJ3Q`;

    axios.get(geocodeURL).then((response) =>{
        if (response.data.status === 'ZERO_RESULTS'){
            throw new Error('Unable to found that address');
        }
        var lt = response.data.results[0].geometry.location.lat;
        var lg = response.data.results[0].geometry.location.lng;
        weatherUrl = `https://api.darksky.net/forecast/af1a15a527b00a1d367d16bef61188fc/${lt},${lg}`
        console.log(response.data.results[0].formatted_address);
        return axios.get(weatherUrl);

    }).then ((response) => {
        var weatherType = response.data.currently.precipType;
        var temperature = ((5 / 9) * (response.data.currently.temperature - 32)).toFixed(2);
        var apparentTemperature = ((5 / 9) * (response.data.currently.apparentTemperature - 32)).toFixed(2);
        console.log(
`Weather for the ${Date()}.
Its currently ${temperature}, but feels like ${apparentTemperature}, behind the windows ${weatherType}
Have a great day man(girl?).`)
    }).catch((e) =>{
        if (e.code === 'ENOTFOUND')
        { console.log('Unnable to connect to api servers');
        } else {
            console.log(e.message);
        }
    });
setTimeout(() => console.log('________Stop work___________'), 2500)