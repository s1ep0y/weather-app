const request = require('request');

var geocodeAdress = (address) => {
    return new Promise ((resolve, reject) =>{
        setTimeout(() =>{
            var encodedAddress = encodeURIComponent(address);
            request({
            url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=AIzaSyCgZsXBjz9jlzZpDQutB1iuiNN6bWrJJ3Q`,
            json: true
            }, (error, response, body) =>{
                if (body.status === "OK"){
                resolve ({
                    Address: body.results[0].formatted_address,
                    Latitude: body.results[0].geometry.location.lat,
                    Longitude: body.results[0].geometry.location.lng,})
                } else if (body.status === 'ZERO_RESULTS') {
                    reject('Unable to find that adress');
                } else if (error){
                    reject('Unable to connect to google serivce');
                }
            })
        } , 2000)
    })
}

geocodeAdress('197183').then((location) =>{
    console.log(JSON.stringify(location, undefined, 2))
}, (errorMessage) =>{
    console.log(errorMessage);
})