console.log('________Starting Workd__________')

const yargs = require('yargs');
const geocode = require ('./geocode/geocode.js')
const weather = require ('./weather/weather.js')


const argv = yargs
    .options({
        a: {
            demand: true,
            alias : 'address',
            describe: 'Adress to fetch weater for',
            string: true,
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

weatherout = (lt, lg) => {
    weather.getWeather(lt, lg, (errorMessage, weatherResults) => {
        if (errorMessage) {
            console.log(errorMessage);
        } else {
            if (weatherResults.WeatherType != undefined){
            console.log(`After the windows ${weatherResults.WeatherType}`);};
            console.log(`Temperature: ${weatherResults.Temperature} celsius`);
            console.log(`Feels like ${weatherResults.apparentTemperature} celsius`)}
        }
    );
}

geocode.geocodeback(argv.address, (errorMessage, results) => {
    if (errorMessage) {
        console.log (errorMessage);
    } else {
        console.log(results.Address);
        weatherout(results.Latitude, results.Longitude);
    }
});

setTimeout(() => console.log('________Stop work___________'), 2000)